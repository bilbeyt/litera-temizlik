﻿namespace Litera_Temizlik
{
    partial class CompanyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CompanyForm));
            this.labelName = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.labelID = new System.Windows.Forms.Label();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.buttonInsert = new System.Windows.Forms.Button();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.dataGridViewCompany = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.actionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.homeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.areaActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.containerTypeActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recyclingOrderTypeActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vehicleActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.companyActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.personelActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCompany)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(52, 88);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(35, 13);
            this.labelName.TabIndex = 6;
            this.labelName.Text = "Name";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(121, 81);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(100, 20);
            this.textBoxName.TabIndex = 7;
            // 
            // labelID
            // 
            this.labelID.AutoSize = true;
            this.labelID.Location = new System.Drawing.Point(55, 123);
            this.labelID.Name = "labelID";
            this.labelID.Size = new System.Drawing.Size(18, 13);
            this.labelID.TabIndex = 8;
            this.labelID.Text = "ID";
            // 
            // textBoxID
            // 
            this.textBoxID.Location = new System.Drawing.Point(121, 115);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.ReadOnly = true;
            this.textBoxID.Size = new System.Drawing.Size(100, 20);
            this.textBoxID.TabIndex = 9;
            // 
            // buttonSearch
            // 
            this.buttonSearch.Image = global::Litera_Temizlik.Properties.Resources.Search_Filled;
            this.buttonSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSearch.Location = new System.Drawing.Point(272, 78);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(75, 23);
            this.buttonSearch.TabIndex = 10;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // buttonInsert
            // 
            this.buttonInsert.Image = global::Litera_Temizlik.Properties.Resources.Save;
            this.buttonInsert.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonInsert.Location = new System.Drawing.Point(43, 181);
            this.buttonInsert.Name = "buttonInsert";
            this.buttonInsert.Size = new System.Drawing.Size(75, 23);
            this.buttonInsert.TabIndex = 11;
            this.buttonInsert.Text = "Insert";
            this.buttonInsert.UseVisualStyleBackColor = true;
            this.buttonInsert.Click += new System.EventHandler(this.buttonInsert_Click);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Image = global::Litera_Temizlik.Properties.Resources.Available_Updates_2;
            this.buttonUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonUpdate.Location = new System.Drawing.Point(189, 181);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdate.TabIndex = 12;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Image = global::Litera_Temizlik.Properties.Resources.Delete;
            this.buttonDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonDelete.Location = new System.Drawing.Point(306, 181);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(75, 23);
            this.buttonDelete.TabIndex = 13;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // dataGridViewCompany
            // 
            this.dataGridViewCompany.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewCompany.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCompany.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridViewCompany.Location = new System.Drawing.Point(0, 232);
            this.dataGridViewCompany.Name = "dataGridViewCompany";
            this.dataGridViewCompany.ReadOnly = true;
            this.dataGridViewCompany.Size = new System.Drawing.Size(452, 150);
            this.dataGridViewCompany.TabIndex = 14;
            this.dataGridViewCompany.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewCompany_RowHeaderMouseClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.actionsToolStripMenuItem,
            this.logoutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(452, 24);
            this.menuStrip1.TabIndex = 15;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // actionsToolStripMenuItem
            // 
            this.actionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.homeToolStripMenuItem,
            this.areaActionsToolStripMenuItem,
            this.containerTypeActionsToolStripMenuItem,
            this.recyclingOrderTypeActionsToolStripMenuItem,
            this.vehicleActionsToolStripMenuItem,
            this.companyActionsToolStripMenuItem,
            this.personelActionsToolStripMenuItem,
            this.userActionsToolStripMenuItem});
            this.actionsToolStripMenuItem.Image = global::Litera_Temizlik.Properties.Resources.Drag_List_Down_Filled;
            this.actionsToolStripMenuItem.Name = "actionsToolStripMenuItem";
            this.actionsToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.actionsToolStripMenuItem.Text = "Actions";
            // 
            // homeToolStripMenuItem
            // 
            this.homeToolStripMenuItem.Name = "homeToolStripMenuItem";
            this.homeToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.homeToolStripMenuItem.Text = "Home";
            this.homeToolStripMenuItem.Click += new System.EventHandler(this.homeToolStripMenuItem_Click);
            // 
            // areaActionsToolStripMenuItem
            // 
            this.areaActionsToolStripMenuItem.Name = "areaActionsToolStripMenuItem";
            this.areaActionsToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.areaActionsToolStripMenuItem.Text = "Area Actions";
            this.areaActionsToolStripMenuItem.Click += new System.EventHandler(this.areaActionsToolStripMenuItem_Click);
            // 
            // containerTypeActionsToolStripMenuItem
            // 
            this.containerTypeActionsToolStripMenuItem.Name = "containerTypeActionsToolStripMenuItem";
            this.containerTypeActionsToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.containerTypeActionsToolStripMenuItem.Text = "Container Type Actions";
            this.containerTypeActionsToolStripMenuItem.Click += new System.EventHandler(this.containerTypeActionsToolStripMenuItem_Click);
            // 
            // recyclingOrderTypeActionsToolStripMenuItem
            // 
            this.recyclingOrderTypeActionsToolStripMenuItem.Name = "recyclingOrderTypeActionsToolStripMenuItem";
            this.recyclingOrderTypeActionsToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.recyclingOrderTypeActionsToolStripMenuItem.Text = "Recycling Order Type Actions";
            this.recyclingOrderTypeActionsToolStripMenuItem.Click += new System.EventHandler(this.recyclingOrderTypeActionsToolStripMenuItem_Click);
            // 
            // vehicleActionsToolStripMenuItem
            // 
            this.vehicleActionsToolStripMenuItem.Name = "vehicleActionsToolStripMenuItem";
            this.vehicleActionsToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.vehicleActionsToolStripMenuItem.Text = "Vehicle Actions";
            this.vehicleActionsToolStripMenuItem.Click += new System.EventHandler(this.vehicleActionsToolStripMenuItem_Click);
            // 
            // companyActionsToolStripMenuItem
            // 
            this.companyActionsToolStripMenuItem.Name = "companyActionsToolStripMenuItem";
            this.companyActionsToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.companyActionsToolStripMenuItem.Text = "Company Actions";
            // 
            // personelActionsToolStripMenuItem
            // 
            this.personelActionsToolStripMenuItem.Name = "personelActionsToolStripMenuItem";
            this.personelActionsToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.personelActionsToolStripMenuItem.Text = "Personel Actions";
            this.personelActionsToolStripMenuItem.Click += new System.EventHandler(this.personelActionsToolStripMenuItem_Click);
            // 
            // userActionsToolStripMenuItem
            // 
            this.userActionsToolStripMenuItem.Name = "userActionsToolStripMenuItem";
            this.userActionsToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.userActionsToolStripMenuItem.Text = "User Actions";
            this.userActionsToolStripMenuItem.Click += new System.EventHandler(this.userActionsToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.logoutToolStripMenuItem.Image = global::Litera_Temizlik.Properties.Resources.Exit_Filled;
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // CompanyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 382);
            this.Controls.Add(this.dataGridViewCompany);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.buttonInsert);
            this.Controls.Add(this.buttonSearch);
            this.Controls.Add(this.textBoxID);
            this.Controls.Add(this.labelID);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "CompanyForm";
            this.Text = "CompanyForm";
            this.Load += new System.EventHandler(this.CompanyForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCompany)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label labelID;
        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Button buttonInsert;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.DataGridView dataGridViewCompany;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem actionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem homeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem areaActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recyclingOrderTypeActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vehicleActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem companyActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem personelActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem containerTypeActionsToolStripMenuItem;
    }
}