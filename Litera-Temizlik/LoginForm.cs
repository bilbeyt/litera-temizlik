﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Security.Cryptography;

namespace Litera_Temizlik
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        public SqlConnection conn;

        private void buttonSubmit_Click(object sender, EventArgs e)
        {
            string username = textBoxNickname.Text;
            string password = textBoxPassword.Text;
            if (username == "" || password == "")
            {
                MessageBox.Show("Nickname or password is empty. Please enter these informations");
            }
            else {
                string connectionstring = Settings.connection_string.ToString();
                conn = new SqlConnection(connectionstring);
                string queryset = "SELECT * FROM Users WHERE nickname=@nickname";
                
                SqlCommand cmd = new SqlCommand(queryset, conn);
                cmd.Parameters.AddWithValue("@nickname", username);
            
                try
                {
                    conn.Open();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adp.Fill(ds);
                 
                    conn.Close();          
                    var data = Encoding.ASCII.GetBytes(password);
                    var sha1 = new SHA1CryptoServiceProvider();
                    var sha1data = sha1.ComputeHash(data);              
                    byte[] data2 = Encoding.ASCII.GetBytes(ds.Tables[0].Rows[0][6].ToString());
                    bool result = sha1data.ToString().SequenceEqual(data2.ToString());
                    if (result)
                    {
                        MessageBox.Show("Login successfully");
                        this.Hide();
                        Main frm = new Main();
                        frm.Show();
                    }
                    else {
                        MessageBox.Show("Login Failed");
                    }           
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Errors[0].Message);
                    conn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    conn.Close();
                }
            }
        }
    }
}
