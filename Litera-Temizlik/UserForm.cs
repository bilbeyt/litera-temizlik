﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Security.Cryptography;

namespace Litera_Temizlik
{
    public partial class UserForm : Form
    {
        public UserForm()
        {
            InitializeComponent();
        }

        public SqlConnection conn;

        private void dataGridViewUser_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            textBoxNickname.Text = dataGridViewUser.Rows[e.RowIndex].Cells[2].Value.ToString();
            textBoxName.Text = dataGridViewUser.Rows[e.RowIndex].Cells[0].Value.ToString();
            textBoxSurname.Text = dataGridViewUser.Rows[e.RowIndex].Cells[1].Value.ToString();
            textBoxPassword.Text = dataGridViewUser.Rows[e.RowIndex].Cells[6].Value.ToString();
            textBoxID.Text = dataGridViewUser.Rows[e.RowIndex].Cells[3].Value.ToString();
            checkBoxActive.Checked = Convert.ToBoolean(dataGridViewUser.Rows[e.RowIndex].Cells[7].Value.ToString());
            labelID.Show();
            textBoxID.Show();
        }

        private void UserForm_Load(object sender, EventArgs e)
        {
            dataGridViewUser.Hide();
            labelID.Hide();
            textBoxID.Hide();
            checkBoxActive.CheckState = CheckState.Indeterminate;
            string connectionstring = Settings.connection_string.ToString();
            conn = new SqlConnection(connectionstring);
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
         
            if (this.textBoxName.Text == "" && this.textBoxNickname.Text == "" && this.textBoxSurname.Text == "" && this.checkBoxActive.CheckState == CheckState.Indeterminate)
            {  
                show_all();
            }
            else
            {
                string queryset = "SELECT * FROM Users WHERE ";
                if (this.textBoxName.Text != "") {
                    queryset = queryset + "name='" + this.textBoxName.Text + "'";
                }
                if (this.textBoxSurname.Text != "") {
                    queryset = queryset + " surname='" + this.textBoxSurname.Text + "'";
                }
                if (this.textBoxNickname.Text != "") {
                    queryset = queryset + " nickname='" + this.textBoxNickname.Text + "'";
                }
                if (this.checkBoxActive.CheckState != CheckState.Indeterminate) {
                    queryset = queryset + " active='" + this.checkBoxActive.Checked + "'";
                }

                try
                {
                    conn.Open();
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(queryset, conn);
                    adp.Fill(dt);
                    dataGridViewUser.DataSource = dt;
                    dataGridViewUser.Show();
                    clean_parameters();
                   
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Errors[0].Message);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                conn.Close();
            }
        }

        private void buttonInsert_Click(object sender, EventArgs e)
        {
            string queryset = "INSERT INTO Users(name,surname,nickname,created,password,active) VALUES(@name,@surname,@nickname,@created,@password,@active)";
            try
            {
                conn.Open();
                var data = Encoding.ASCII.GetBytes(textBoxPassword.ToString());
                var sha1 = new SHA1CryptoServiceProvider();
                var sha1data = sha1.ComputeHash(data);
                SqlCommand cmd = new SqlCommand(queryset, conn);
                cmd.Parameters.AddWithValue("@name", this.textBoxName.Text);
                cmd.Parameters.AddWithValue("@surname", this.textBoxSurname.Text);
                cmd.Parameters.AddWithValue("@nickname", this.textBoxNickname.Text);
                cmd.Parameters.AddWithValue("@created", DateTime.Now);
                cmd.Parameters.AddWithValue("@password", sha1data);
                cmd.Parameters.AddWithValue("@active", this.checkBoxActive.Checked);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Added");
                conn.Close();
                clean_parameters();
                show_all();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            string queryset = "UPDATE Users SET name=@name, surname=@surname, nickname=@nickname, password=@password, active=@active WHERE ID=@id";
            try
            {
                conn.Open();
                var data = Encoding.ASCII.GetBytes(textBoxPassword.ToString());
                var sha1 = new SHA1CryptoServiceProvider();
                var sha1data = sha1.ComputeHash(data);
                int id = Convert.ToInt32(textBoxID.Text.ToString());
                SqlCommand cmd = new SqlCommand(queryset, conn);
                cmd.Parameters.AddWithValue("@name", this.textBoxName.Text);
                cmd.Parameters.AddWithValue("@surname", this.textBoxSurname.Text);
                cmd.Parameters.AddWithValue("@nickname", this.textBoxNickname.Text);
                cmd.Parameters.AddWithValue("@password", sha1data);
                cmd.Parameters.AddWithValue("@active", this.checkBoxActive.Checked);
                cmd.Parameters.AddWithValue("@id", id);  
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Updated");
                conn.Close();
                clean_parameters();
                show_all();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            string queryset = "DELETE Users WHERE ID=@id";
            try
            {
                conn.Open();
                int id = Convert.ToInt32(this.textBoxID.Text);
                SqlCommand cmd = new SqlCommand(queryset, conn);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Deleted");
                conn.Close();
                clean_parameters();
                show_all();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
        }
        private void show_all() {
            string queryset = "SELECT * FROM Users";
            try
            {
                conn.Open();
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(queryset, conn);
                adp.Fill(dt);
                dataGridViewUser.DataSource = dt;
                dataGridViewUser.Show();
                
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            conn.Close();
        }
        private void clean_parameters() {
            checkBoxActive.Checked = false;
            textBoxID.Text = "";
            textBoxName.Text = "";
            textBoxNickname.Text = "";
            textBoxPassword.Text = "";
            textBoxSurname.Text = "";
        }

        private void homeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Main frm = new Main();
            frm.Show();
        }

        private void areaActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            AreaForm frm = new AreaForm();
            frm.Show();
        }

        private void recyclingOrderTypeActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            RecyclingOrderTypeForm frm = new RecyclingOrderTypeForm();
            frm.Show();
        }

        private void workingTypeActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            WorkingTypeForm frm = new WorkingTypeForm();
            frm.Show();
        }

        private void vehicleActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            VehicleForm frm = new VehicleForm();
            frm.Show();
        }

        private void companyActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            CompanyForm frm = new CompanyForm();
            frm.Show();
        }

        private void personelActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            PersonelForm frm = new PersonelForm();
            frm.Show();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Logout Successfully");
            this.Hide();
            LoginForm frm = new LoginForm();
            frm.Show();
        }

        private void containerTypeActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            ContainerTypeForm frm = new ContainerTypeForm();
            frm.Show();
        }
    }
}
