﻿namespace Litera_Temizlik
{
    partial class RecyclingOrderTypeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RecyclingOrderTypeForm));
            this.labelTypeName = new System.Windows.Forms.Label();
            this.labelActive = new System.Windows.Forms.Label();
            this.textBoxOrderType = new System.Windows.Forms.TextBox();
            this.checkBoxActive = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.labelID = new System.Windows.Forms.Label();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.dataGridViewRecyclingOrderType = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.actionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.homeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.areaActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.containerTypeActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.workingTypeActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vehicleActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.companyActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRecyclingOrderType)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelTypeName
            // 
            this.labelTypeName.AutoSize = true;
            this.labelTypeName.Location = new System.Drawing.Point(43, 51);
            this.labelTypeName.Name = "labelTypeName";
            this.labelTypeName.Size = new System.Drawing.Size(60, 13);
            this.labelTypeName.TabIndex = 0;
            this.labelTypeName.Text = "Order Type";
            // 
            // labelActive
            // 
            this.labelActive.AutoSize = true;
            this.labelActive.Location = new System.Drawing.Point(46, 82);
            this.labelActive.Name = "labelActive";
            this.labelActive.Size = new System.Drawing.Size(41, 13);
            this.labelActive.TabIndex = 1;
            this.labelActive.Text = "Default";
            // 
            // textBoxOrderType
            // 
            this.textBoxOrderType.Location = new System.Drawing.Point(123, 51);
            this.textBoxOrderType.Name = "textBoxOrderType";
            this.textBoxOrderType.Size = new System.Drawing.Size(100, 20);
            this.textBoxOrderType.TabIndex = 2;
            // 
            // checkBoxActive
            // 
            this.checkBoxActive.AutoSize = true;
            this.checkBoxActive.Location = new System.Drawing.Point(126, 81);
            this.checkBoxActive.Name = "checkBoxActive";
            this.checkBoxActive.Size = new System.Drawing.Size(15, 14);
            this.checkBoxActive.TabIndex = 3;
            this.checkBoxActive.ThreeState = true;
            this.checkBoxActive.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Image = global::Litera_Temizlik.Properties.Resources.Save;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(49, 156);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Insert";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Image = global::Litera_Temizlik.Properties.Resources.Available_Updates_2;
            this.buttonUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonUpdate.Location = new System.Drawing.Point(215, 156);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdate.TabIndex = 5;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Image = global::Litera_Temizlik.Properties.Resources.Delete;
            this.buttonDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonDelete.Location = new System.Drawing.Point(395, 156);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(75, 23);
            this.buttonDelete.TabIndex = 6;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // labelID
            // 
            this.labelID.AutoSize = true;
            this.labelID.Location = new System.Drawing.Point(49, 112);
            this.labelID.Name = "labelID";
            this.labelID.Size = new System.Drawing.Size(18, 13);
            this.labelID.TabIndex = 7;
            this.labelID.Text = "ID";
            // 
            // textBoxID
            // 
            this.textBoxID.Location = new System.Drawing.Point(126, 112);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.ReadOnly = true;
            this.textBoxID.Size = new System.Drawing.Size(100, 20);
            this.textBoxID.TabIndex = 8;
            // 
            // buttonSearch
            // 
            this.buttonSearch.Image = global::Litera_Temizlik.Properties.Resources.Search_Filled;
            this.buttonSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSearch.Location = new System.Drawing.Point(315, 47);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(75, 23);
            this.buttonSearch.TabIndex = 9;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // dataGridViewRecyclingOrderType
            // 
            this.dataGridViewRecyclingOrderType.AllowUserToOrderColumns = true;
            this.dataGridViewRecyclingOrderType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewRecyclingOrderType.Location = new System.Drawing.Point(93, 217);
            this.dataGridViewRecyclingOrderType.Name = "dataGridViewRecyclingOrderType";
            this.dataGridViewRecyclingOrderType.ReadOnly = true;
            this.dataGridViewRecyclingOrderType.Size = new System.Drawing.Size(349, 150);
            this.dataGridViewRecyclingOrderType.TabIndex = 10;
            this.dataGridViewRecyclingOrderType.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewRecyclingOrderType_RowHeaderMouseClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.actionsToolStripMenuItem,
            this.logoutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(574, 24);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // actionsToolStripMenuItem
            // 
            this.actionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.homeToolStripMenuItem,
            this.areaActionsToolStripMenuItem,
            this.containerTypeActionsToolStripMenuItem,
            this.workingTypeActionsToolStripMenuItem,
            this.vehicleActionsToolStripMenuItem,
            this.companyActionsToolStripMenuItem,
            this.userActionsToolStripMenuItem});
            this.actionsToolStripMenuItem.Image = global::Litera_Temizlik.Properties.Resources.Drag_List_Down_Filled;
            this.actionsToolStripMenuItem.Name = "actionsToolStripMenuItem";
            this.actionsToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.actionsToolStripMenuItem.Text = "Actions";
            // 
            // homeToolStripMenuItem
            // 
            this.homeToolStripMenuItem.Name = "homeToolStripMenuItem";
            this.homeToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.homeToolStripMenuItem.Text = "Home";
            this.homeToolStripMenuItem.Click += new System.EventHandler(this.homeToolStripMenuItem_Click);
            // 
            // areaActionsToolStripMenuItem
            // 
            this.areaActionsToolStripMenuItem.Name = "areaActionsToolStripMenuItem";
            this.areaActionsToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.areaActionsToolStripMenuItem.Text = "Area Actions";
            this.areaActionsToolStripMenuItem.Click += new System.EventHandler(this.areaActionsToolStripMenuItem_Click);
            // 
            // containerTypeActionsToolStripMenuItem
            // 
            this.containerTypeActionsToolStripMenuItem.Name = "containerTypeActionsToolStripMenuItem";
            this.containerTypeActionsToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.containerTypeActionsToolStripMenuItem.Text = "Container Type Actions";
            this.containerTypeActionsToolStripMenuItem.Click += new System.EventHandler(this.containerTypeActionsToolStripMenuItem_Click);
            // 
            // workingTypeActionsToolStripMenuItem
            // 
            this.workingTypeActionsToolStripMenuItem.Name = "workingTypeActionsToolStripMenuItem";
            this.workingTypeActionsToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.workingTypeActionsToolStripMenuItem.Text = "Working Type Actions";
            this.workingTypeActionsToolStripMenuItem.Click += new System.EventHandler(this.workingTypeActionsToolStripMenuItem_Click);
            // 
            // vehicleActionsToolStripMenuItem
            // 
            this.vehicleActionsToolStripMenuItem.Name = "vehicleActionsToolStripMenuItem";
            this.vehicleActionsToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.vehicleActionsToolStripMenuItem.Text = "Vehicle Actions";
            this.vehicleActionsToolStripMenuItem.Click += new System.EventHandler(this.vehicleActionsToolStripMenuItem_Click);
            // 
            // companyActionsToolStripMenuItem
            // 
            this.companyActionsToolStripMenuItem.Name = "companyActionsToolStripMenuItem";
            this.companyActionsToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.companyActionsToolStripMenuItem.Text = "Company Actions";
            this.companyActionsToolStripMenuItem.Click += new System.EventHandler(this.companyActionsToolStripMenuItem_Click);
            // 
            // userActionsToolStripMenuItem
            // 
            this.userActionsToolStripMenuItem.Name = "userActionsToolStripMenuItem";
            this.userActionsToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.userActionsToolStripMenuItem.Text = "User Actions";
            this.userActionsToolStripMenuItem.Click += new System.EventHandler(this.userActionsToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.logoutToolStripMenuItem.Image = global::Litera_Temizlik.Properties.Resources.Exit_Filled;
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // RecyclingOrderTypeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 379);
            this.Controls.Add(this.dataGridViewRecyclingOrderType);
            this.Controls.Add(this.buttonSearch);
            this.Controls.Add(this.textBoxID);
            this.Controls.Add(this.labelID);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.checkBoxActive);
            this.Controls.Add(this.textBoxOrderType);
            this.Controls.Add(this.labelActive);
            this.Controls.Add(this.labelTypeName);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "RecyclingOrderTypeForm";
            this.Text = "RecyclingOrderTypeForm";
            this.Load += new System.EventHandler(this.RecyclingOrderTypeForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRecyclingOrderType)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTypeName;
        private System.Windows.Forms.Label labelActive;
        private System.Windows.Forms.TextBox textBoxOrderType;
        private System.Windows.Forms.CheckBox checkBoxActive;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Label labelID;
        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.DataGridView dataGridViewRecyclingOrderType;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem actionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem homeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem areaActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem containerTypeActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem workingTypeActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vehicleActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem companyActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userActionsToolStripMenuItem;
    }
}