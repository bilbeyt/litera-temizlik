﻿namespace Litera_Temizlik
{
    partial class PersonelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PersonelForm));
            this.labelCompany = new System.Windows.Forms.Label();
            this.labelUser = new System.Windows.Forms.Label();
            this.labelExplanation = new System.Windows.Forms.Label();
            this.richTextBoxExplanation = new System.Windows.Forms.RichTextBox();
            this.comboBoxCompany = new System.Windows.Forms.ComboBox();
            this.comboBoxUser = new System.Windows.Forms.ComboBox();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.buttonInsert = new System.Windows.Forms.Button();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.dataGridViewPersonel = new System.Windows.Forms.DataGridView();
            this.labelID = new System.Windows.Forms.Label();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.actionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.homeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.areaActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.containerTypeActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recyclingOrderTypeActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.workingTypeActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vehicleActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.companyActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPersonel)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelCompany
            // 
            this.labelCompany.AutoSize = true;
            this.labelCompany.Location = new System.Drawing.Point(37, 116);
            this.labelCompany.Name = "labelCompany";
            this.labelCompany.Size = new System.Drawing.Size(51, 13);
            this.labelCompany.TabIndex = 0;
            this.labelCompany.Text = "Company";
            // 
            // labelUser
            // 
            this.labelUser.AutoSize = true;
            this.labelUser.Location = new System.Drawing.Point(40, 165);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(29, 13);
            this.labelUser.TabIndex = 1;
            this.labelUser.Text = "User";
            // 
            // labelExplanation
            // 
            this.labelExplanation.AutoSize = true;
            this.labelExplanation.Location = new System.Drawing.Point(40, 217);
            this.labelExplanation.Name = "labelExplanation";
            this.labelExplanation.Size = new System.Drawing.Size(62, 13);
            this.labelExplanation.TabIndex = 2;
            this.labelExplanation.Text = "Explanation";
            // 
            // richTextBoxExplanation
            // 
            this.richTextBoxExplanation.Location = new System.Drawing.Point(131, 214);
            this.richTextBoxExplanation.Name = "richTextBoxExplanation";
            this.richTextBoxExplanation.Size = new System.Drawing.Size(186, 93);
            this.richTextBoxExplanation.TabIndex = 8;
            this.richTextBoxExplanation.Text = "";
            // 
            // comboBoxCompany
            // 
            this.comboBoxCompany.FormattingEnabled = true;
            this.comboBoxCompany.Location = new System.Drawing.Point(131, 107);
            this.comboBoxCompany.Name = "comboBoxCompany";
            this.comboBoxCompany.Size = new System.Drawing.Size(121, 21);
            this.comboBoxCompany.TabIndex = 9;
            // 
            // comboBoxUser
            // 
            this.comboBoxUser.FormattingEnabled = true;
            this.comboBoxUser.Location = new System.Drawing.Point(131, 156);
            this.comboBoxUser.Name = "comboBoxUser";
            this.comboBoxUser.Size = new System.Drawing.Size(121, 21);
            this.comboBoxUser.TabIndex = 10;
            // 
            // buttonSearch
            // 
            this.buttonSearch.Image = global::Litera_Temizlik.Properties.Resources.Search_Filled;
            this.buttonSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSearch.Location = new System.Drawing.Point(353, 156);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(75, 23);
            this.buttonSearch.TabIndex = 11;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // buttonInsert
            // 
            this.buttonInsert.Image = global::Litera_Temizlik.Properties.Resources.Save;
            this.buttonInsert.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonInsert.Location = new System.Drawing.Point(43, 341);
            this.buttonInsert.Name = "buttonInsert";
            this.buttonInsert.Size = new System.Drawing.Size(75, 23);
            this.buttonInsert.TabIndex = 12;
            this.buttonInsert.Text = "Insert";
            this.buttonInsert.UseVisualStyleBackColor = true;
            this.buttonInsert.Click += new System.EventHandler(this.buttonInsert_Click);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Image = global::Litera_Temizlik.Properties.Resources.Available_Updates_2;
            this.buttonUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonUpdate.Location = new System.Drawing.Point(190, 341);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdate.TabIndex = 13;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Image = global::Litera_Temizlik.Properties.Resources.Delete;
            this.buttonDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonDelete.Location = new System.Drawing.Point(342, 341);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(75, 23);
            this.buttonDelete.TabIndex = 14;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // dataGridViewPersonel
            // 
            this.dataGridViewPersonel.AllowUserToOrderColumns = true;
            this.dataGridViewPersonel.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewPersonel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPersonel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridViewPersonel.Location = new System.Drawing.Point(0, 399);
            this.dataGridViewPersonel.Name = "dataGridViewPersonel";
            this.dataGridViewPersonel.ReadOnly = true;
            this.dataGridViewPersonel.Size = new System.Drawing.Size(680, 150);
            this.dataGridViewPersonel.TabIndex = 15;
            this.dataGridViewPersonel.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewPersonel_RowHeaderMouseClick);
            // 
            // labelID
            // 
            this.labelID.AutoSize = true;
            this.labelID.Location = new System.Drawing.Point(40, 74);
            this.labelID.Name = "labelID";
            this.labelID.Size = new System.Drawing.Size(18, 13);
            this.labelID.TabIndex = 16;
            this.labelID.Text = "ID";
            // 
            // textBoxID
            // 
            this.textBoxID.Location = new System.Drawing.Point(131, 71);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.ReadOnly = true;
            this.textBoxID.Size = new System.Drawing.Size(100, 20);
            this.textBoxID.TabIndex = 17;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.actionsToolStripMenuItem,
            this.logoutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(680, 24);
            this.menuStrip1.TabIndex = 18;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // actionsToolStripMenuItem
            // 
            this.actionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.homeToolStripMenuItem,
            this.areaActionsToolStripMenuItem,
            this.containerTypeActionsToolStripMenuItem,
            this.recyclingOrderTypeActionsToolStripMenuItem,
            this.workingTypeActionsToolStripMenuItem,
            this.vehicleActionsToolStripMenuItem,
            this.companyActionsToolStripMenuItem,
            this.userActionsToolStripMenuItem});
            this.actionsToolStripMenuItem.Image = global::Litera_Temizlik.Properties.Resources.Drag_List_Down_Filled;
            this.actionsToolStripMenuItem.Name = "actionsToolStripMenuItem";
            this.actionsToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.actionsToolStripMenuItem.Text = "Actions";
            // 
            // homeToolStripMenuItem
            // 
            this.homeToolStripMenuItem.Name = "homeToolStripMenuItem";
            this.homeToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.homeToolStripMenuItem.Text = "Home";
            this.homeToolStripMenuItem.Click += new System.EventHandler(this.homeToolStripMenuItem_Click);
            // 
            // areaActionsToolStripMenuItem
            // 
            this.areaActionsToolStripMenuItem.Name = "areaActionsToolStripMenuItem";
            this.areaActionsToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.areaActionsToolStripMenuItem.Text = "Area Actions";
            this.areaActionsToolStripMenuItem.Click += new System.EventHandler(this.areaActionsToolStripMenuItem_Click);
            // 
            // containerTypeActionsToolStripMenuItem
            // 
            this.containerTypeActionsToolStripMenuItem.Name = "containerTypeActionsToolStripMenuItem";
            this.containerTypeActionsToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.containerTypeActionsToolStripMenuItem.Text = "Container Type Actions";
            this.containerTypeActionsToolStripMenuItem.Click += new System.EventHandler(this.containerTypeActionsToolStripMenuItem_Click);
            // 
            // recyclingOrderTypeActionsToolStripMenuItem
            // 
            this.recyclingOrderTypeActionsToolStripMenuItem.Name = "recyclingOrderTypeActionsToolStripMenuItem";
            this.recyclingOrderTypeActionsToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.recyclingOrderTypeActionsToolStripMenuItem.Text = "Recycling Order Type Actions";
            this.recyclingOrderTypeActionsToolStripMenuItem.Click += new System.EventHandler(this.recyclingOrderTypeActionsToolStripMenuItem_Click);
            // 
            // workingTypeActionsToolStripMenuItem
            // 
            this.workingTypeActionsToolStripMenuItem.Name = "workingTypeActionsToolStripMenuItem";
            this.workingTypeActionsToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.workingTypeActionsToolStripMenuItem.Text = "Working Type Actions";
            this.workingTypeActionsToolStripMenuItem.Click += new System.EventHandler(this.workingTypeActionsToolStripMenuItem_Click);
            // 
            // vehicleActionsToolStripMenuItem
            // 
            this.vehicleActionsToolStripMenuItem.Name = "vehicleActionsToolStripMenuItem";
            this.vehicleActionsToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.vehicleActionsToolStripMenuItem.Text = "Vehicle Actions";
            this.vehicleActionsToolStripMenuItem.Click += new System.EventHandler(this.vehicleActionsToolStripMenuItem_Click);
            // 
            // companyActionsToolStripMenuItem
            // 
            this.companyActionsToolStripMenuItem.Name = "companyActionsToolStripMenuItem";
            this.companyActionsToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.companyActionsToolStripMenuItem.Text = "Company Actions";
            this.companyActionsToolStripMenuItem.Click += new System.EventHandler(this.companyActionsToolStripMenuItem_Click);
            // 
            // userActionsToolStripMenuItem
            // 
            this.userActionsToolStripMenuItem.Name = "userActionsToolStripMenuItem";
            this.userActionsToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.userActionsToolStripMenuItem.Text = "User Actions";
            this.userActionsToolStripMenuItem.Click += new System.EventHandler(this.userActionsToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.logoutToolStripMenuItem.Image = global::Litera_Temizlik.Properties.Resources.Exit_Filled;
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // PersonelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 549);
            this.Controls.Add(this.textBoxID);
            this.Controls.Add(this.labelID);
            this.Controls.Add(this.dataGridViewPersonel);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.buttonInsert);
            this.Controls.Add(this.buttonSearch);
            this.Controls.Add(this.comboBoxUser);
            this.Controls.Add(this.comboBoxCompany);
            this.Controls.Add(this.richTextBoxExplanation);
            this.Controls.Add(this.labelExplanation);
            this.Controls.Add(this.labelUser);
            this.Controls.Add(this.labelCompany);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "PersonelForm";
            this.Text = "PersonelForm";
            this.Load += new System.EventHandler(this.PersonelForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPersonel)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelCompany;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.Label labelExplanation;
        private System.Windows.Forms.RichTextBox richTextBoxExplanation;
        private System.Windows.Forms.ComboBox comboBoxCompany;
        private System.Windows.Forms.ComboBox comboBoxUser;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Button buttonInsert;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.DataGridView dataGridViewPersonel;
        private System.Windows.Forms.Label labelID;
        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem actionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem homeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem areaActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem containerTypeActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recyclingOrderTypeActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem workingTypeActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vehicleActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem companyActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
    }
}