﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Litera_Temizlik
{
    public partial class AreaForm : Form
    {
        public AreaForm()
        {
            InitializeComponent();
        }
        public SqlConnection conn;

        private void buttonInsert_Click(object sender, EventArgs e)
        {
            string queryset = "INSERT INTO Areas(name) VALUES(@name)";
            
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(queryset, conn);
                cmd.Parameters.AddWithValue("@name", this.textBoxName.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Added");
                clean_parameters();
                conn.Close();
                show_all();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            string queryset = "UPDATE Areas set Name=@name WHERE ID=@id";
            try
            {
                conn.Open();
                int id = Convert.ToInt32(this.textBoxID.Text);
                SqlCommand cmd = new SqlCommand(queryset, conn);
                cmd.Parameters.AddWithValue("@name", this.textBoxName.Text);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Updated");
                clean_parameters();
                conn.Close();
                show_all();
                
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
            
        }
        
        private void dataGridViewArea_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            labelID.Show();
            textBoxID.Show();
            textBoxID.Text = dataGridViewArea.Rows[e.RowIndex].Cells[1].Value.ToString();
            textBoxName.Text = dataGridViewArea.Rows[e.RowIndex].Cells[0].Value.ToString();
        }

        private void AreaForm_Load(object sender, EventArgs e)
        {
            labelID.Hide();
            textBoxID.Hide();
            dataGridViewArea.Hide();
            string connectionstring = Settings.connection_string.ToString();
            conn = new SqlConnection(connectionstring);
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            string queryset = "DELETE Areas WHERE ID=@id";
            
            try
            {
                conn.Open();
                int id = Convert.ToInt32(this.textBoxID.Text);
                SqlCommand cmd = new SqlCommand(queryset, conn);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Deleted");
                clean_parameters();
                conn.Close();
                show_all();
                
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
        }

        private void show_all() {
            string queryset = "SELECT * FROM Areas";
            try
            {
                conn.Open();
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(queryset, conn);
                adp.Fill(dt);
                dataGridViewArea.DataSource = dt;
                dataGridViewArea.Show();
                
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            conn.Close();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            if (this.textBoxName.Text == "")
            {
                show_all();
            }
            else
            {
                string queryset = "SELECT * FROM Areas WHERE name='" + this.textBoxName.Text + "'";
                try
                {
                    conn.Open();
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(queryset, conn);
                    adp.Fill(dt);
                    dataGridViewArea.DataSource = dt;
                    dataGridViewArea.Show();
                    clean_parameters();
                    
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Errors[0].Message);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                conn.Close();
            }
        }

        private void clean_parameters() {
            labelID.Hide();
            textBoxID.Hide();
            textBoxName.Text = "";
        }

        private void homeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Main frm = new Main();
            frm.Show();
        }

        private void recyclingOrderTypeActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            RecyclingOrderTypeForm frm = new RecyclingOrderTypeForm();
            frm.Show();
        }

        private void workingTypeActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            WorkingTypeForm frm = new WorkingTypeForm();
            frm.Show();
        }

        private void vehicleActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            VehicleForm frm = new VehicleForm();
            frm.Show();
        }

        private void companyActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            CompanyForm frm = new CompanyForm();
            frm.Show();
        }

        private void personelActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            PersonelForm frm = new PersonelForm();
            frm.Show();
        }

        private void userActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            UserForm frm = new UserForm();
            frm.Show();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Logout Successfully");
            this.Hide();
            LoginForm frm = new LoginForm();
            frm.Show();
        }

        private void containerTypeActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            ContainerTypeForm frm = new ContainerTypeForm();
            frm.Show();
        }

        private void logoutToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            LoginForm frm = new LoginForm();
            frm.Show();
        }
    }
}
