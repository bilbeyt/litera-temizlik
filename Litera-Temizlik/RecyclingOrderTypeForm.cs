﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Litera_Temizlik
{
    public partial class RecyclingOrderTypeForm : Form
    {
        public RecyclingOrderTypeForm()
        {
            InitializeComponent();
        }

        public SqlConnection conn;

        private void RecyclingOrderTypeForm_Load(object sender, EventArgs e)
        {
            dataGridViewRecyclingOrderType.Hide();
            labelID.Hide();
            textBoxID.Hide();
            checkBoxActive.CheckState = CheckState.Indeterminate;
            string connectionstring = Settings.connection_string.ToString();
            conn = new SqlConnection(connectionstring);
        }

        private void dataGridViewRecyclingOrderType_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            textBoxID.Text = dataGridViewRecyclingOrderType.Rows[e.RowIndex].Cells[2].Value.ToString();
            textBoxOrderType.Text = dataGridViewRecyclingOrderType.Rows[e.RowIndex].Cells[0].Value.ToString();
            checkBoxActive.Checked = Convert.ToBoolean(dataGridViewRecyclingOrderType.Rows[e.RowIndex].Cells[1].Value.ToString());
            textBoxID.Show();
            labelID.Show();
        }

        private void show_all() {
            string queryset = "SELECT * FROM Recycling_Order_Types";
            try
            {
                conn.Open();
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(queryset, conn);
                adp.Fill(dt);
                dataGridViewRecyclingOrderType.DataSource = dt;
                dataGridViewRecyclingOrderType.Show();
               
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            conn.Close();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            if (this.textBoxOrderType.Text == "" && this.checkBoxActive.CheckState == CheckState.Indeterminate)
            {
                show_all();
            }
            else
            {
                string queryset = "SELECT * FROM Recycling_Order_Types WHERE ";
                if( this.textBoxOrderType.Text != ""){
                    queryset = queryset + "type='"+this.textBoxOrderType.Text + "'";
                }
                if(this.checkBoxActive.CheckState != CheckState.Indeterminate){
                    queryset = queryset + " active='" + this.checkBoxActive.Checked + "'";
                }
                try
                {
                    conn.Open();
                    MessageBox.Show(queryset);
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(queryset, conn);
                    adp.Fill(dt);
                    dataGridViewRecyclingOrderType.DataSource = dt;
                    dataGridViewRecyclingOrderType.Show();
                    textBoxOrderType.Text = "";
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Errors[0].Message);
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                   
                }
                conn.Close();
            }
        }

        private void clean_parameters() {
            textBoxID.Text = "";
            textBoxOrderType.Text = "";
            checkBoxActive.Checked = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string queryset = "INSERT INTO Recycling_Order_Types(type, active) VALUES(@type, @active)";  
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(queryset, conn);
                cmd.Parameters.AddWithValue("@type", this.textBoxOrderType.Text);
                cmd.Parameters.AddWithValue("@active", this.checkBoxActive.Checked);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Added");    
                conn.Close();
                clean_parameters();
                show_all();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            string queryset = "UPDATE Recycling_Order_Types SET type=@type, active=@active WHERE ID=@id";
            try
            {
                conn.Open();
                int id = Convert.ToInt32(textBoxID.Text);
                SqlCommand cmd = new SqlCommand(queryset, conn);
                cmd.Parameters.AddWithValue("@type", this.textBoxOrderType.Text);
                cmd.Parameters.AddWithValue("@active", this.checkBoxActive.Checked);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Updated");      
                conn.Close();
                clean_parameters();
                show_all();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            string queryset = "DELETE Recycling_Order_Types WHERE ID=@id";
            
            try
            {
                conn.Open();
                int id = Convert.ToInt32(this.textBoxID.Text);
                SqlCommand cmd = new SqlCommand(queryset, conn);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Deleted");            
                conn.Close();
                clean_parameters();
                show_all();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
        }

        private void homeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Main frm = new Main();
            frm.Show();
        }

        private void areaActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            AreaForm frm = new AreaForm();
            frm.Show();
        }

        private void workingTypeActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            WorkingTypeForm frm = new WorkingTypeForm();
            frm.Show();
        }

        private void vehicleActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            VehicleForm frm = new VehicleForm();
            frm.Show();
        }

        private void companyActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            CompanyForm frm = new CompanyForm();
            frm.Show();
        }

        private void personelActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            PersonelForm frm = new PersonelForm();
            frm.Show();
        }

        private void userActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            UserForm frm = new UserForm();
            frm.Show();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Logout Successfully");
            this.Hide();
            LoginForm frm = new LoginForm();
            frm.Show();
        }

        private void containerTypeActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            ContainerTypeForm frm = new ContainerTypeForm();
            frm.Show();
        }
    }
}
