﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Litera_Temizlik
{
    public partial class PersonelForm : Form
    {
        public PersonelForm()
        {
            InitializeComponent();
        }

        public SqlConnection conn;

        private void dataGridViewPersonel_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            textBoxID.Text = dataGridViewPersonel.Rows[e.RowIndex].Cells[3].Value.ToString();
            richTextBoxExplanation.Text = dataGridViewPersonel.Rows[e.RowIndex].Cells[2].Value.ToString();
            int company_id = Convert.ToInt32(dataGridViewPersonel.Rows[e.RowIndex].Cells[0].Value);
            int user_id = Convert.ToInt32(dataGridViewPersonel.Rows[e.RowIndex].Cells[1].Value);
            comboBoxCompany.SelectedIndex = comboBoxCompany.Items.IndexOf(get_company_name(company_id));
            comboBoxUser.SelectedIndex = comboBoxUser.Items.IndexOf(get_user_name(user_id));
            labelID.Show();
            textBoxID.Show();
        }

        private void PersonelForm_Load(object sender, EventArgs e)
        {
            dataGridViewPersonel.Hide();
            labelID.Hide();
            textBoxID.Hide();
            string connectionstring = Settings.connection_string.ToString();
            conn = new SqlConnection(connectionstring);
            try
            {
                string queryset = "SELECT * FROM Companies";
                conn.Open();
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(queryset, conn);
                adp.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    comboBoxCompany.Items.Add(row[1].ToString());
                }
                comboBoxCompany.Items.Insert(0, "Select Company");
                comboBoxCompany.SelectedIndex = 0;
                queryset = "SELECT * FROM Users";
                DataTable dt2 = new DataTable();
                SqlDataAdapter adp2 = new SqlDataAdapter(queryset, conn);
                adp2.Fill(dt2);
                foreach (DataRow row in dt2.Rows) {
                    comboBoxUser.Items.Add(row[0].ToString());
                }
                comboBoxUser.Items.Insert(0, "Select User");
                comboBoxUser.SelectedIndex = 0;

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            conn.Close();
        }

        private void buttonInsert_Click(object sender, EventArgs e)
        {
            string queryset = "INSERT INTO Personels(company_id, user_id, explanation) VALUES(@company_id,@user_id,@explanation)";
            try
            {
                int company_id = get_company_id(this.comboBoxCompany.Text);
                int user_id = get_user_id(this.comboBoxUser.Text);
                conn.Open();
                SqlCommand cmd = new SqlCommand(queryset, conn);
                cmd.Parameters.AddWithValue("@company_id", company_id);
                cmd.Parameters.AddWithValue("@user_id", user_id);
                cmd.Parameters.AddWithValue("@explanation", this.richTextBoxExplanation.Text);        
                cmd.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show("Successfully Added");
                clean_parameters();
                show_all();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            string queryset = "UPDATE Personels SET user_id=@user_id, explanation=@explanation, company_id=@company_id WHERE id=@id";
            try
            {
                int company_id = get_company_id(this.comboBoxCompany.Text);
                int user_id = get_user_id(this.comboBoxUser.Text);
                conn.Open();
                int id = Convert.ToInt32(textBoxID.Text);
                SqlCommand cmd = new SqlCommand(queryset, conn);
                cmd.Parameters.AddWithValue("@user_id", user_id);
                cmd.Parameters.AddWithValue("@explanation", this.richTextBoxExplanation.Text);
                cmd.Parameters.AddWithValue("@company_id", company_id);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show("Successfully Updated");
                clean_parameters();
                show_all();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            string queryset = "DELETE Personels WHERE ID=@id";

            try
            {
                conn.Open();
                int id = Convert.ToInt32(this.textBoxID.Text);
                SqlCommand cmd = new SqlCommand(queryset, conn);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show("Successfully Deleted");
                clean_parameters();
                show_all();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            if (comboBoxUser.SelectedIndex == 0 && comboBoxCompany.SelectedIndex == 0 && richTextBoxExplanation.Text == "")
            {
                show_all();
            }
            else {
                string queryset = "SELECT * FROM Personels WHERE ";
                if (comboBoxCompany.SelectedIndex != 0) {
                    queryset = queryset + " company_id='" + comboBoxCompany.Text + "'";
                }
                if (comboBoxUser.SelectedIndex != 0) {
                    queryset = queryset + " user_id='" + comboBoxUser.Text + "'";
                }
                if (richTextBoxExplanation.Text != "") {
                    queryset = queryset + " explanation='" + richTextBoxExplanation.Text + "'";
                }
                try
                {
                    conn.Open();
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(queryset, conn);
                    conn.Close();
                    adp.Fill(dt);
                    dataGridViewPersonel.DataSource = dt;
                    dataGridViewPersonel.Show();

                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Errors[0].Message);
                    conn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    conn.Close();
                }
            }
        }

        private void clean_parameters() {
            comboBoxCompany.SelectedIndex = 0;
            comboBoxUser.SelectedIndex = 0;
            richTextBoxExplanation.Text = "";
        }

        private void show_all() {
            string queryset = "SELECT * FROM Personels";
            try
            {
                conn.Open();
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(queryset, conn);
                conn.Close();
                adp.Fill(dt);
                dataGridViewPersonel.DataSource = dt;
                dataGridViewPersonel.Show();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }

        }

        private string get_company_name(int id) {
            string queryset = "SELECT * FROM Companies WHERE id='" + id.ToString() + "'";
            try
            {
                conn.Open();
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(queryset, conn);
                conn.Close();
                adp.Fill(dt);
                return dt.Rows[0][1].ToString();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
            return "";
        }

        private string get_user_name(int id) {
            string queryset = "SELECT * FROM Users WHERE id='" + id.ToString() + "'";
            try
            {
                conn.Open();
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(queryset, conn);
                conn.Close();
                adp.Fill(dt);
                return dt.Rows[0][0].ToString();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
            return "";
        }

        private int get_company_id(string name) {
            string queryset = "SELECT * FROM Companies WHERE name='" + name + "'";
            try
            {
                conn.Open();
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(queryset, conn);
                conn.Close();
                adp.Fill(dt);
                return Convert.ToInt32(dt.Rows[0][0].ToString());
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
            return 0;
        }

        private int get_user_id(string name) {
            string queryset = "SELECT * FROM Users WHERE name='" + name + "'";
            try
            {
                conn.Open();
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(queryset, conn);
                conn.Close();
                adp.Fill(dt);
                return Convert.ToInt32(dt.Rows[0][3].ToString());
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
            return 0;
        }
    

        private void homeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Main frm = new Main();
            frm.Show();
        }

        private void areaActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            AreaForm frm = new AreaForm();
            frm.Show();
        }

        private void recyclingOrderTypeActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            RecyclingOrderTypeForm frm = new RecyclingOrderTypeForm();
            frm.Show();
        }

        private void workingTypeActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            WorkingTypeForm frm = new WorkingTypeForm();
            frm.Show();
        }

        private void vehicleActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            VehicleForm frm = new VehicleForm();
            frm.Show();
        }

        private void companyActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            CompanyForm frm = new CompanyForm();
            frm.Show();
        }

        private void userActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            UserForm frm = new UserForm();
            frm.Show();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Logout Successfully");
            this.Hide();
            LoginForm frm = new LoginForm();
            frm.Show();
        }

        private void containerTypeActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            ContainerTypeForm frm = new ContainerTypeForm();
            frm.Show();
        }
       
    }
}
