﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Litera_Temizlik
{
    public partial class VehicleForm : Form
    {
        public VehicleForm()
        {
            InitializeComponent();
        }

        public SqlConnection conn;

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            textBoxID.Text = dataGridViewVehicle.Rows[e.RowIndex].Cells[3].Value.ToString();
            textBoxPlate.Text = dataGridViewVehicle.Rows[e.RowIndex].Cells[0].Value.ToString();
            richTextBoxExplanation.Text = dataGridViewVehicle.Rows[e.RowIndex].Cells[1].Value.ToString();
            int company_id = Convert.ToInt32(dataGridViewVehicle.Rows[e.RowIndex].Cells[2].Value);
            comboBoxCompany.SelectedIndex = comboBoxCompany.Items.IndexOf(get_company_name(company_id));
            labelID.Show();
            textBoxID.Show();
        }

        private void VehicleForm_Load(object sender, EventArgs e)
        {
            dataGridViewVehicle.Hide();
            labelID.Hide();
            textBoxID.Hide();
            string connectionstring = Settings.connection_string.ToString();
            conn = new SqlConnection(connectionstring);
            try
            {
                string queryset = "SELECT * FROM Companies";
                conn.Open();
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(queryset, conn);
                adp.Fill(dt);       
                foreach(DataRow row in dt.Rows){
                    comboBoxCompany.Items.Add(row[1].ToString());
                }
                comboBoxCompany.Items.Insert(0, "Select Company");
                comboBoxCompany.SelectedIndex = 0;            
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            conn.Close();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            if (richTextBoxExplanation.Text == "" && textBoxPlate.Text == "" && comboBoxCompany.SelectedIndex == 0)
            {
                show_all();
            }
            else {
                string queryset = "SELECT * FROM Vehicles WHERE ";
                if (richTextBoxExplanation.Text != "") {
                    queryset = queryset + "explanation='" + richTextBoxExplanation.Text + "'";
                }
                if (textBoxPlate.Text != "") {
                    queryset = queryset + " plate='" + textBoxPlate.Text + "'";
                }
                
                if (comboBoxCompany.SelectedIndex != 0) {
                    int id = search_name_id(comboBoxCompany.Text);
                    queryset = queryset + " company_id='" + id.ToString() + "'";
                }
                try
                {
                    conn.Open();
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(queryset, conn);
                    conn.Close();
                    adp.Fill(dt);
                    dataGridViewVehicle.DataSource = dt;
                    dataGridViewVehicle.Show();
                    clean_parameters();
                    
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Errors[0].Message);
                    conn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    conn.Close();
                }
             
            }
        }

        private void buttonInsert_Click(object sender, EventArgs e)
        {
            string queryset = "INSERT INTO Vehicles(plate,explanation,company_id) VALUES(@plate,@explanation,@company_id)";
            try
            {
                int company_id = search_name_id(this.comboBoxCompany.Text);
                conn.Open();
                SqlCommand cmd = new SqlCommand(queryset, conn);
                cmd.Parameters.AddWithValue("@plate", this.textBoxPlate.Text);
                cmd.Parameters.AddWithValue("@explanation", this.richTextBoxExplanation.Text);
                cmd.Parameters.AddWithValue("@company_id", company_id);
                cmd.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show("Successfully Added");         
                clean_parameters();
                show_all();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
     
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            string queryset = "UPDATE Vehicles SET plate=@plate, explanation=@explanation, company_id=@company_id WHERE id=@id";
            try
            {
                int company_id = search_name_id(this.comboBoxCompany.Text);
                conn.Open();
                int id = Convert.ToInt32(textBoxID.Text);
                SqlCommand cmd = new SqlCommand(queryset, conn);
                cmd.Parameters.AddWithValue("@plate", this.textBoxPlate.Text);
                cmd.Parameters.AddWithValue("@explanation", this.richTextBoxExplanation.Text);
                cmd.Parameters.AddWithValue("@company_id", company_id);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show("Successfully Updated");           
                clean_parameters();
                show_all();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
        
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            string queryset = "DELETE Vehicles WHERE ID=@id";

            try
            {
                conn.Open();
                int id = Convert.ToInt32(this.textBoxID.Text);
                SqlCommand cmd = new SqlCommand(queryset, conn);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show("Successfully Deleted");                   
                clean_parameters();
                show_all();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
  
        }
        private void show_all() {
            string queryset = "SELECT * FROM Vehicles";
            try
            {
                conn.Open();
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(queryset, conn);
                conn.Close();
                adp.Fill(dt);
                dataGridViewVehicle.DataSource = dt;
                dataGridViewVehicle.Show();
                
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
        }
        private void clean_parameters() {
            textBoxID.Text = "";
            textBoxPlate.Text = "";
            richTextBoxExplanation.Text = "";
            comboBoxCompany.SelectedIndex = 0;
        }
        private string get_company_name(int id) {
            string queryset = "SELECT * FROM Companies WHERE id='" + id.ToString() + "'";
            try
            {
                conn.Open();
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(queryset, conn);
                conn.Close();
                adp.Fill(dt);               
                
                return dt.Rows[0][1].ToString();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
            return "";
        }
        private int search_name_id(string name){
            string queryset = "SELECT * FROM Companies WHERE name='" + name + "'";
            try
            {
                conn.Open();
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(queryset, conn);
                conn.Close();
                adp.Fill(dt);               
                
             
                return Convert.ToInt32(dt.Rows[0][0].ToString());
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
            return 0;
        }

        private void homeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Main frm = new Main();
            frm.Show();
        }

        private void areaActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            AreaForm frm = new AreaForm();
            frm.Show();
        }

        private void recyclingOrderTypeActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            RecyclingOrderTypeForm frm = new RecyclingOrderTypeForm();
            frm.Show();
        }

        private void workingTypeActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            WorkingTypeForm frm = new WorkingTypeForm();
            frm.Show();
        }

        private void companyActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            CompanyForm frm = new CompanyForm();
            frm.Show();
        }

        private void personelActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            PersonelForm frm = new PersonelForm();
            frm.Show();
        }

        private void userActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            UserForm frm = new UserForm();
            frm.Show();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Logout Successfully");
            this.Hide();
            LoginForm frm = new LoginForm();
            frm.Show();
        }

        private void containerTypeActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            ContainerTypeForm frm = new ContainerTypeForm();
            frm.Show();
        }

       
    }
}
