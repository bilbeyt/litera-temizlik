﻿namespace Litera_Temizlik
{
    partial class UserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserForm));
            this.labelNickname = new System.Windows.Forms.Label();
            this.textBoxNickname = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.labelSurname = new System.Windows.Forms.Label();
            this.textBoxSurname = new System.Windows.Forms.TextBox();
            this.labelID = new System.Windows.Forms.Label();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.labelActive = new System.Windows.Forms.Label();
            this.checkBoxActive = new System.Windows.Forms.CheckBox();
            this.labelPassword = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.dataGridViewUser = new System.Windows.Forms.DataGridView();
            this.buttonInsert = new System.Windows.Forms.Button();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.actionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.homeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.areaActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.containerTypeActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recyclingOrderTypeActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.workingTypeActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vehicleActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.companyActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.personelActionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUser)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelNickname
            // 
            this.labelNickname.AutoSize = true;
            this.labelNickname.Location = new System.Drawing.Point(33, 58);
            this.labelNickname.Name = "labelNickname";
            this.labelNickname.Size = new System.Drawing.Size(60, 13);
            this.labelNickname.TabIndex = 0;
            this.labelNickname.Text = "Nick Name";
            // 
            // textBoxNickname
            // 
            this.textBoxNickname.Location = new System.Drawing.Point(160, 58);
            this.textBoxNickname.Name = "textBoxNickname";
            this.textBoxNickname.Size = new System.Drawing.Size(100, 20);
            this.textBoxNickname.TabIndex = 1;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(33, 130);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(35, 13);
            this.labelName.TabIndex = 2;
            this.labelName.Text = "Name";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(160, 130);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(100, 20);
            this.textBoxName.TabIndex = 3;
            // 
            // labelSurname
            // 
            this.labelSurname.AutoSize = true;
            this.labelSurname.Location = new System.Drawing.Point(33, 182);
            this.labelSurname.Name = "labelSurname";
            this.labelSurname.Size = new System.Drawing.Size(49, 13);
            this.labelSurname.TabIndex = 4;
            this.labelSurname.Text = "Surname";
            // 
            // textBoxSurname
            // 
            this.textBoxSurname.Location = new System.Drawing.Point(160, 175);
            this.textBoxSurname.Name = "textBoxSurname";
            this.textBoxSurname.Size = new System.Drawing.Size(100, 20);
            this.textBoxSurname.TabIndex = 5;
            // 
            // labelID
            // 
            this.labelID.AutoSize = true;
            this.labelID.Location = new System.Drawing.Point(33, 245);
            this.labelID.Name = "labelID";
            this.labelID.Size = new System.Drawing.Size(18, 13);
            this.labelID.TabIndex = 6;
            this.labelID.Text = "ID";
            // 
            // textBoxID
            // 
            this.textBoxID.Location = new System.Drawing.Point(160, 245);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.ReadOnly = true;
            this.textBoxID.Size = new System.Drawing.Size(100, 20);
            this.textBoxID.TabIndex = 7;
            // 
            // labelActive
            // 
            this.labelActive.AutoSize = true;
            this.labelActive.Location = new System.Drawing.Point(36, 213);
            this.labelActive.Name = "labelActive";
            this.labelActive.Size = new System.Drawing.Size(37, 13);
            this.labelActive.TabIndex = 10;
            this.labelActive.Text = "Active";
            // 
            // checkBoxActive
            // 
            this.checkBoxActive.AutoSize = true;
            this.checkBoxActive.Location = new System.Drawing.Point(160, 213);
            this.checkBoxActive.Name = "checkBoxActive";
            this.checkBoxActive.Size = new System.Drawing.Size(15, 14);
            this.checkBoxActive.TabIndex = 11;
            this.checkBoxActive.ThreeState = true;
            this.checkBoxActive.UseVisualStyleBackColor = true;
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(36, 93);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(53, 13);
            this.labelPassword.TabIndex = 12;
            this.labelPassword.Text = "Password";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(160, 93);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(100, 20);
            this.textBoxPassword.TabIndex = 13;
            this.textBoxPassword.UseSystemPasswordChar = true;
            // 
            // dataGridViewUser
            // 
            this.dataGridViewUser.AllowUserToOrderColumns = true;
            this.dataGridViewUser.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewUser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewUser.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridViewUser.Location = new System.Drawing.Point(0, 317);
            this.dataGridViewUser.Name = "dataGridViewUser";
            this.dataGridViewUser.ReadOnly = true;
            this.dataGridViewUser.Size = new System.Drawing.Size(692, 150);
            this.dataGridViewUser.TabIndex = 14;
            this.dataGridViewUser.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewUser_RowHeaderMouseClick);
            // 
            // buttonInsert
            // 
            this.buttonInsert.Image = global::Litera_Temizlik.Properties.Resources.Save;
            this.buttonInsert.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonInsert.Location = new System.Drawing.Point(35, 283);
            this.buttonInsert.Name = "buttonInsert";
            this.buttonInsert.Size = new System.Drawing.Size(75, 23);
            this.buttonInsert.TabIndex = 15;
            this.buttonInsert.Text = "Insert";
            this.buttonInsert.UseVisualStyleBackColor = true;
            this.buttonInsert.Click += new System.EventHandler(this.buttonInsert_Click);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Image = global::Litera_Temizlik.Properties.Resources.Available_Updates_2;
            this.buttonUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonUpdate.Location = new System.Drawing.Point(209, 283);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdate.TabIndex = 16;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Image = global::Litera_Temizlik.Properties.Resources.Delete;
            this.buttonDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonDelete.Location = new System.Drawing.Point(413, 283);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(75, 23);
            this.buttonDelete.TabIndex = 17;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonSearch
            // 
            this.buttonSearch.Image = global::Litera_Temizlik.Properties.Resources.Search_Filled;
            this.buttonSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSearch.Location = new System.Drawing.Point(413, 130);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(75, 23);
            this.buttonSearch.TabIndex = 18;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.actionsToolStripMenuItem,
            this.logoutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(692, 24);
            this.menuStrip1.TabIndex = 19;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // actionsToolStripMenuItem
            // 
            this.actionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.homeToolStripMenuItem,
            this.areaActionsToolStripMenuItem,
            this.containerTypeActionsToolStripMenuItem,
            this.recyclingOrderTypeActionsToolStripMenuItem,
            this.workingTypeActionsToolStripMenuItem,
            this.vehicleActionsToolStripMenuItem,
            this.companyActionsToolStripMenuItem,
            this.personelActionsToolStripMenuItem});
            this.actionsToolStripMenuItem.Image = global::Litera_Temizlik.Properties.Resources.Drag_List_Down_Filled;
            this.actionsToolStripMenuItem.Name = "actionsToolStripMenuItem";
            this.actionsToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.actionsToolStripMenuItem.Text = "Actions";
            // 
            // homeToolStripMenuItem
            // 
            this.homeToolStripMenuItem.Name = "homeToolStripMenuItem";
            this.homeToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.homeToolStripMenuItem.Text = "Home";
            this.homeToolStripMenuItem.Click += new System.EventHandler(this.homeToolStripMenuItem_Click);
            // 
            // areaActionsToolStripMenuItem
            // 
            this.areaActionsToolStripMenuItem.Name = "areaActionsToolStripMenuItem";
            this.areaActionsToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.areaActionsToolStripMenuItem.Text = "Area Actions";
            this.areaActionsToolStripMenuItem.Click += new System.EventHandler(this.areaActionsToolStripMenuItem_Click);
            // 
            // containerTypeActionsToolStripMenuItem
            // 
            this.containerTypeActionsToolStripMenuItem.Name = "containerTypeActionsToolStripMenuItem";
            this.containerTypeActionsToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.containerTypeActionsToolStripMenuItem.Text = "Container Type Actions";
            this.containerTypeActionsToolStripMenuItem.Click += new System.EventHandler(this.containerTypeActionsToolStripMenuItem_Click);
            // 
            // recyclingOrderTypeActionsToolStripMenuItem
            // 
            this.recyclingOrderTypeActionsToolStripMenuItem.Name = "recyclingOrderTypeActionsToolStripMenuItem";
            this.recyclingOrderTypeActionsToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.recyclingOrderTypeActionsToolStripMenuItem.Text = "Recycling Order Type Actions";
            this.recyclingOrderTypeActionsToolStripMenuItem.Click += new System.EventHandler(this.recyclingOrderTypeActionsToolStripMenuItem_Click);
            // 
            // workingTypeActionsToolStripMenuItem
            // 
            this.workingTypeActionsToolStripMenuItem.Name = "workingTypeActionsToolStripMenuItem";
            this.workingTypeActionsToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.workingTypeActionsToolStripMenuItem.Text = "Working Type Actions";
            this.workingTypeActionsToolStripMenuItem.Click += new System.EventHandler(this.workingTypeActionsToolStripMenuItem_Click);
            // 
            // vehicleActionsToolStripMenuItem
            // 
            this.vehicleActionsToolStripMenuItem.Name = "vehicleActionsToolStripMenuItem";
            this.vehicleActionsToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.vehicleActionsToolStripMenuItem.Text = "Vehicle Actions";
            this.vehicleActionsToolStripMenuItem.Click += new System.EventHandler(this.vehicleActionsToolStripMenuItem_Click);
            // 
            // companyActionsToolStripMenuItem
            // 
            this.companyActionsToolStripMenuItem.Name = "companyActionsToolStripMenuItem";
            this.companyActionsToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.companyActionsToolStripMenuItem.Text = "Company Actions";
            this.companyActionsToolStripMenuItem.Click += new System.EventHandler(this.companyActionsToolStripMenuItem_Click);
            // 
            // personelActionsToolStripMenuItem
            // 
            this.personelActionsToolStripMenuItem.Name = "personelActionsToolStripMenuItem";
            this.personelActionsToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.personelActionsToolStripMenuItem.Text = "Personel Actions";
            this.personelActionsToolStripMenuItem.Click += new System.EventHandler(this.personelActionsToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.logoutToolStripMenuItem.Image = global::Litera_Temizlik.Properties.Resources.Exit_Filled;
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // UserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(692, 467);
            this.Controls.Add(this.buttonSearch);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.buttonInsert);
            this.Controls.Add(this.dataGridViewUser);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.labelPassword);
            this.Controls.Add(this.checkBoxActive);
            this.Controls.Add(this.labelActive);
            this.Controls.Add(this.textBoxID);
            this.Controls.Add(this.labelID);
            this.Controls.Add(this.textBoxSurname);
            this.Controls.Add(this.labelSurname);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.textBoxNickname);
            this.Controls.Add(this.labelNickname);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "UserForm";
            this.Text = "UserForm";
            this.Load += new System.EventHandler(this.UserForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUser)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNickname;
        private System.Windows.Forms.TextBox textBoxNickname;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label labelSurname;
        private System.Windows.Forms.TextBox textBoxSurname;
        private System.Windows.Forms.Label labelID;
        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.Label labelActive;
        private System.Windows.Forms.CheckBox checkBoxActive;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.DataGridView dataGridViewUser;
        private System.Windows.Forms.Button buttonInsert;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem actionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem homeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem areaActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem containerTypeActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recyclingOrderTypeActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem workingTypeActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vehicleActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem companyActionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem personelActionsToolStripMenuItem;
    }
}