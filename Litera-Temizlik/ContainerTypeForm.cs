﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Litera_Temizlik
{
    public partial class ContainerTypeForm : Form
    {
        public ContainerTypeForm()
        {
            InitializeComponent();
        }

        public SqlConnection conn;

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            textBoxID.Text = dataGridViewContainer.Rows[e.RowIndex].Cells[9].Value.ToString();
            textBoxContainerCode.Text = dataGridViewContainer.Rows[e.RowIndex].Cells[1].Value.ToString();
            textBoxMark.Text = dataGridViewContainer.Rows[e.RowIndex].Cells[2].Value.ToString();
            textBoxModel.Text = dataGridViewContainer.Rows[e.RowIndex].Cells[3].Value.ToString();
            textBoxTotalIn.Text = dataGridViewContainer.Rows[e.RowIndex].Cells[6].Value.ToString();
            textBoxTotalOut.Text = dataGridViewContainer.Rows[e.RowIndex].Cells[7].Value.ToString();
            textBoxCost.Text = dataGridViewContainer.Rows[e.RowIndex].Cells[5].Value.ToString();
            textBoxRemaining.Text = dataGridViewContainer.Rows[e.RowIndex].Cells[8].Value.ToString();
            textBoxTechFeature.Text = dataGridViewContainer.Rows[e.RowIndex].Cells[4].Value.ToString();
            textBoxName.Text = dataGridViewContainer.Rows[e.RowIndex].Cells[0].Value.ToString();
            textBoxID.Show();
            labelID.Show();
            labelRemaning.Show();
            textBoxRemaining.Show();
        }

        private void ContainerTypeForm_Load(object sender, EventArgs e)
        {
            dataGridViewContainer.Hide();
            labelID.Hide();
            textBoxID.Hide();
            labelRemaning.Hide();
            textBoxRemaining.Hide();
            string connectionstring = Settings.connection_string.ToString();
            conn = new SqlConnection(connectionstring);
        }

        private void buttonInsert_Click(object sender, EventArgs e)
        {
            string queryset = "INSERT INTO Containers(name,container_code,mark,model,tech_feature,cost,total_in,total_out,remaining) VALUES(@name,@container_code,@mark,@model,@tech_feature,@cost,@total_in,@total_out,@remaining)";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(queryset, conn);
                int remaining = Convert.ToInt32(textBoxTotalIn.Text) - Convert.ToInt32(textBoxTotalOut.Text);
                cmd.Parameters.AddWithValue("@name", this.textBoxName.Text);
                cmd.Parameters.AddWithValue("@container_code", this.textBoxContainerCode.Text);
                cmd.Parameters.AddWithValue("@mark", this.textBoxMark.Text);
                cmd.Parameters.AddWithValue("@model", this.textBoxModel.Text);
                cmd.Parameters.AddWithValue("@tech_feature", this.textBoxTechFeature.Text);
                cmd.Parameters.AddWithValue("@total_in", this.textBoxTotalIn.Text);
                cmd.Parameters.AddWithValue("@cost", this.textBoxCost.Text);
                cmd.Parameters.AddWithValue("@total_out", this.textBoxTotalOut.Text);
                cmd.Parameters.AddWithValue("@remaining", remaining.ToString());
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Added");
                clean_parameters();
                conn.Close();
                labelID.Hide();
                textBoxID.Hide();
                textBoxRemaining.Hide();
                labelRemaning.Hide();
                show_all();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            string queryset = "UPDATE Containers set name=@name, container_code=@container_code, mark=@mark, model=@model, total_in=@total_in, total_out=@total_out, remaining=@remaining, cost=@cost, tech_feature=@tech_feature WHERE ID=@id";
            
            try
            {
                conn.Open();
                int id = Convert.ToInt32(this.textBoxID.Text);
                int remaining = Convert.ToInt32(textBoxTotalIn.Text) - Convert.ToInt32(textBoxTotalOut.Text);
                SqlCommand cmd = new SqlCommand(queryset, conn);
                cmd.Parameters.AddWithValue("@name", this.textBoxName.Text);
                cmd.Parameters.AddWithValue("@container_code", this.textBoxContainerCode.Text);
                cmd.Parameters.AddWithValue("@mark", this.textBoxMark.Text);
                cmd.Parameters.AddWithValue("@model", this.textBoxModel.Text);
                cmd.Parameters.AddWithValue("@tech_feature", this.textBoxTechFeature.Text);
                cmd.Parameters.AddWithValue("@total_in", this.textBoxTotalIn.Text);
                cmd.Parameters.AddWithValue("@cost", this.textBoxCost.Text);
                cmd.Parameters.AddWithValue("@total_out", this.textBoxTotalOut.Text);
                cmd.Parameters.AddWithValue("@remaining", remaining.ToString());
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Updated");          
                conn.Close();
                clean_parameters();
                labelID.Hide();
                textBoxID.Hide();
                textBoxRemaining.Hide();
                labelRemaning.Hide();
                show_all();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            string queryset = "DELETE Containers WHERE ID=@id";
            
            try
            {
                conn.Open();
                int id = Convert.ToInt32(this.textBoxID.Text);
                SqlCommand cmd = new SqlCommand(queryset, conn);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Deleted");   
                conn.Close();
                clean_parameters();
                labelID.Hide();
                textBoxID.Hide();
                textBoxRemaining.Hide();
                labelRemaning.Hide();
                show_all();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            if (textBoxCost.Text == "" && textBoxContainerCode.Text == "" && textBoxMark.Text == "" &&
                textBoxModel.Text == "" && textBoxName.Text == "" && textBoxTechFeature.Text == "" &&
                textBoxTotalIn.Text == "" && textBoxTotalOut.Text == "")
            {
                show_all();
            }
            else {
                string queryset = "SELECT * FROM Containers WHERE ";
                if (textBoxTotalIn.Text != "") {
                    queryset = queryset + "total_in='" + textBoxTotalIn.Text + "'";
                }
                if (textBoxTotalOut.Text != "") {
                    queryset = queryset + " total_out='" + textBoxTotalOut.Text + "'";
                }
                if (textBoxTechFeature.Text != "") {
                    queryset = queryset + " tech_feature='" + textBoxTechFeature.Text + "'";
                }
                if (textBoxModel.Text != "") {
                    queryset = queryset + " model='" + textBoxModel.Text + "'";
                }
                if (textBoxMark.Text != "") {
                    queryset = queryset + " mark='" + textBoxMark.Text + "'";
                }
                if (textBoxContainerCode.Text != "") {
                    queryset = queryset + " container_code='" + textBoxContainerCode.Text + "'";
                }
                if (textBoxName.Text != "") {
                    queryset = queryset + " name='" + textBoxName.Text + "'";
                }
                try
                {
                    conn.Open();
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(queryset, conn);
                    adp.Fill(dt);
                    dataGridViewContainer.DataSource = dt;
                    dataGridViewContainer.Show();
                    textBoxName.Text = "";
                    
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Errors[0].Message);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                conn.Close();
            }
        }
        private void clean_parameters(){
            textBoxID.Text = "";
            textBoxContainerCode.Text = "";
            textBoxMark.Text = "";
            textBoxModel.Text = "";
            textBoxTotalIn.Text = "";
            textBoxTotalOut.Text = "";
            textBoxCost.Text = "";
            textBoxRemaining.Text = "";
            textBoxTechFeature.Text = "";
            textBoxName.Text = "";
        }
        private void show_all(){
            string queryset = "SELECT * FROM Containers";    
            try
            {
                conn.Open();
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(queryset, conn);
                adp.Fill(dt);
                dataGridViewContainer.DataSource = dt;
                dataGridViewContainer.Show();
                
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Errors[0].Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            conn.Close();
        }

        private void areaActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            AreaForm frm = new AreaForm();
            frm.Show();
        }

        private void recyclingOrderTypeActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            RecyclingOrderTypeForm frm = new RecyclingOrderTypeForm();
            frm.Show();
        }

        private void workingTypeActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            WorkingTypeForm frm = new WorkingTypeForm();
            frm.Show();
        }

        private void vehicleActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            VehicleForm frm = new VehicleForm();
            frm.Show();
        }

        private void companyActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            CompanyForm frm = new CompanyForm();
            frm.Show();
        }

        private void personelActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            PersonelForm frm = new PersonelForm();
            frm.Show();
        }

        private void userActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            UserForm frm = new UserForm();
            frm.Show();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Logout Successfully");
            this.Hide();
            LoginForm frm = new LoginForm();
            frm.Show();
        }

        private void homeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Main frm = new Main();
            frm.Show();
        }

        
    }
}
